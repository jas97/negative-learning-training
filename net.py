import torch
import torch.nn as nn
import torch.nn.functional as F

HIDDEN = 50

class Net(nn.Module):
    def __init__(self, net_type):
        # Feature extraction part
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(1, 10, kernel_size=5)
        self.conv2 = nn.Conv2d(10, 20, kernel_size=5)
        self.conv2_drop = nn.Dropout2d()
        self.fc1 = nn.Linear(320, HIDDEN)

        # normal and inverted features
        self.fc1normal = nn.Linear(320, HIDDEN)
        self.fc1negative = nn.Linear(320, HIDDEN)

        # output layer
        self.fc2 = nn.Linear(HIDDEN, 10)

        # output layers for training normal and inverted features
        self.fc2normal = nn.Linear(HIDDEN, 10)
        self.fc2negative = nn.Linear(HIDDEN, 10)

        self.net_type = net_type

        self.synergy = 'inactive';

    def forward(self, x):
        x = F.relu(F.max_pool2d(self.conv1(x), 2))
        x = F.relu(F.max_pool2d(self.conv2_drop(self.conv2(x)), 2))
        x = x.view(-1, 320)

        if self.synergy == 'synergy':
            x1 = x
            x2 = x.neg()

            x1 = F.tanh(self.fc1normal(x1))
            x2 = F.tanh(self.fc1negative(x2))

            x1 = F.dropout(x1, training=self.training)
            x2 = F.dropout(x2, training=self.training)

            x1 = self.fc2normal(x1)
            x2 = self.fc2negative(x2)

            x = x1 + x2

        else:
            
            # for training on negative features
            if self.net_type == 'negative':
                x = torch.ones_like(x).add(x.neg())
            # no synergy - push through output layer
            if self.synergy == 'inactive':
                x = self.fc2(F.dropout(F.tanh(self.fc1(x)), training=self.training))
            # synergy == normal - push through separate normal output layer
            elif self.synergy == 'normal':
                x = self.fc2normal(F.dropout(F.tanh(self.fc1normal(x)), training=self.training))
            # synergy == negative - push through separate negative output layer    
            elif self.synergy == 'negative':
                x = self.fc2negative(F.dropout(F.tanh(self.fc1negative(x)), training=self.training))

        return F.log_softmax(x, dim=1)
