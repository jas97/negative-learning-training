from __future__ import print_function
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import datasets, transforms
from tqdm import tqdm
import random
import time
import os

# ---------------------------
# Local imports
# ---------------------------
from net import Net
from net import HIDDEN
from blur import GaussianSmoothing
from helperfunctions import write_results_csv, check_class_accuracy, mnist_loader, fetch_custom_test_sets, save_custom_test_sets


print(torch.__version__)

torch.manual_seed(1)

LR = 0.1
MOM = 0.5

def train(model, device, train_loader, optimizer, epoch):
    print("Started epoch: {}".format(epoch))
    model.train()
    for batch_idx, (data, target) in enumerate(train_loader):
        data, target = data.cuda(), target.cuda()
        optimizer.zero_grad()
        output = model(data)
        loss = F.nll_loss(output, target)
        loss.backward()
        optimizer.step()
        if batch_idx % 100 == 0:
            print('[net_type: {}, synergy: {}] Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                model.net_type, model.synergy, epoch, batch_idx * len(data), len(train_loader.dataset),
                100. * batch_idx / len(train_loader), loss.item()))
    print("Finished training for epoch: {} \n".format(epoch))

def test(model, device, test_loader):
    model.eval()
    test_loss = 0
    correct = 0
    with torch.no_grad():
        for data, target in test_loader:
            data, target = data.cuda(), target.cuda()
            output = model(data)
            test_loss += F.nll_loss(output, target, reduction='sum').item()  # sum up batch loss
            pred = output.max(1, keepdim=True)[1]  # get the index of the max log-probability
            correct += pred.eq(target.view_as(pred)).sum().item()

    test_loss /= len(test_loader.dataset)
    print('[net_type: {}, synergy: {}] Test set: Average loss: {:.4f}, Accuracy: {}/{} ({:.0f}%)'.format(
        model.net_type, model.synergy, test_loss, correct, len(test_loader.dataset),
        100. * correct / len(test_loader.dataset)))

    return test_loss, 100. * (correct / len(test_loader.dataset))



use_cuda = torch.cuda.is_available()
#print(f"\n\nUse cuda?{use_cuda}\n\n")
device = torch.cuda.device('cuda' if use_cuda else 'cpu')
kwargs = {'num_workers': 12, 'pin_memory': True} if use_cuda else {}


# ------------------------
# Generate test sets
# ------------------------
train_loader = mnist_loader(train=True)
test_loader = mnist_loader()
test_loader_vertical_cut = mnist_loader()
test_loader_horizontal_cut = mnist_loader()
test_loader_diagonal_cut = mnist_loader()
test_loader_quarter_cut = mnist_loader()
test_loader_triple_cut = [mnist_loader(), mnist_loader(), mnist_loader()]  # 5x5, 7x7 and 9x9
test_loader_triple_cut_noise = [mnist_loader(), mnist_loader(), mnist_loader()]
test_loader_triple_cut_replaced1 = [mnist_loader(), mnist_loader(), mnist_loader()]
test_loader_triple_cut_replaced3 = [mnist_loader(), mnist_loader(), mnist_loader()]
test_loader_triple_cut_blur = [mnist_loader(), mnist_loader(), mnist_loader()]

print('Generating new test sets...')



# ------------------------
# 
# ------------------------
def get_random_pairs(test_loader, num):
    label = test_loader.dataset.test_labels[num]
    while True:
        pairs = []
        while len(set(pairs)) != 3:
            pairs = [random.randint(0, 10000 - 1) for i in range(3)]

        got_duplicate_label = False
        for pair in pairs:
            if test_loader.dataset.test_labels[pair] == label:
                got_duplicate_label = True

        if got_duplicate_label:
            continue
        else:
            return pairs


# -------------------------------------------
# Defines a model for synergy training
# -------------------------------------------
model_synergy = Net('synergy').cuda()

smoothing = GaussianSmoothing(1, 5, 1)

for num in tqdm(range(0, 10000)):
    random_pairs = get_random_pairs(test_loader, num)

    sample = test_loader.dataset.test_data[num].type('torch.FloatTensor')
    sample = F.pad(sample.reshape(1, 1, 28, 28), (2, 2, 2, 2), mode='reflect')
    blur = smoothing(sample).reshape(28, 28)

    for x in range(28):
        for y in range(28):
            if y < 14:
                test_loader_vertical_cut.dataset.test_data[num, x, y] = 0
            if x < 14:
                test_loader_horizontal_cut.dataset.test_data[num, x, y] = 0
            if (x < 14 and y > 14) or (x > 14 and y < 14):
                test_loader_diagonal_cut.dataset.test_data[num, x, y] = 0
            if x < 14 and y < 14:
                test_loader_quarter_cut.dataset.test_data[num, x, y] = 0
            for i in range(3):
                half = i + 2  # squares will have side 2*half + 1
                if (10 - half <= x <= 10 + half and 10 - half <= y <= 10 + half) or (22 - half <= x <= 22 + half and 22 - half <= y <= 22 + half) or (12 - half <= x <= 12 + half and 21 - half <= y <= 21 + half):
                    test_loader_triple_cut[i].dataset.test_data[num, x, y] = 0
                    test_loader_triple_cut_noise[i].dataset.test_data[num, x, y] = random.randint(0, 255)
                    test_loader_triple_cut_replaced1[i].dataset.test_data[num, x, y] = test_loader.dataset.test_data[random_pairs[0], x, y]
                    test_loader_triple_cut_blur[i].dataset.test_data[num, x, y] = blur[x, y]
                    if 10 - half <= x <= 10 + half and 10 - half <= y <= 10 + half:
                        test_loader_triple_cut_replaced3[i].dataset.test_data[num, x, y] = test_loader.dataset.test_data[random_pairs[0], x, y]
                    elif 22 - half <= x <= 22 + half and 22 - half <= y <= 22 + half:
                        test_loader_triple_cut_replaced3[i].dataset.test_data[num, x, y] = test_loader.dataset.test_data[random_pairs[1], x, y]
                    elif 12 - half <= x <= 12 + half and 21 - half <= y <= 21 + half:
                        test_loader_triple_cut_replaced3[i].dataset.test_data[num, x, y] = test_loader.dataset.test_data[random_pairs[2], x, y]


print('\nTest sets generated...\n')

# import sys
# sys.exit(0)

# -------------------------------------------
# Optimizer only over parameters that require grad
# -------------------------------------------
optimizer_synergy = optim.SGD(filter(lambda p: p.requires_grad, model_synergy.parameters()), lr=LR, momentum=MOM)

start_time = time.time()


# -------------------------------------------
# Define the datasets and their names
# -------------------------------------------
models = [model_synergy]

dataset_names = ['Normal:', 'HCUT:', 'VCUT:', 'DCUT:', 'QCUT',
                'TCUT/5:', 'TCUT/7:', 'TCUT/9:', 
                'TCUTB/5', 'TCUTB/7', 'TCUTB/9',
                'TCUTN/5', 'TCUTN/7', 'TCUTN/9',
                'TCUTR1/5', 'TCUTR1/7', 'TCUTR1/9',
                'TCUTR3/5', 'TCUTR3/7', 'TCUTR3/9']

datasets = [test_loader, test_loader_horizontal_cut, test_loader_vertical_cut,
            test_loader_diagonal_cut, test_loader_quarter_cut,
            test_loader_triple_cut[0], test_loader_triple_cut[1], test_loader_triple_cut[2],
            test_loader_triple_cut_blur[0], test_loader_triple_cut_blur[1], test_loader_triple_cut_blur[2], 
            test_loader_triple_cut_noise[0], test_loader_triple_cut_noise[1], test_loader_triple_cut_noise[2], 
            test_loader_triple_cut_replaced1[0], test_loader_triple_cut_replaced1[1], test_loader_triple_cut_replaced1[2],
            test_loader_triple_cut_replaced3[0], test_loader_triple_cut_replaced3[1], test_loader_triple_cut_replaced3[2]]

# datasets = fetch_custom_test_sets()

# Try to save the datasets
# i = 0
# if not os.path.exists(os.path.join(os.getcwd(), 'datasets')):
#     os.makedirs(os.path.join(os.getcwd(), 'datasets'))
# for dtst in datasets:
#     if not os.path.exists(os.path.join(os.getcwd(), 'datasets', str(i) + '.pt')):
#         print("Saving dataset " + str(i))
#         with open(os.path.join(os.getcwd(), 'datasets', str(i) + '.pt'), "wb+") as f:
#             new_test_set = (
#                     dtst.dataset.test_data,
#                     dtst.dataset.test_labels
#                 )
#             torch.save(new_test_set, f)
#             i = i + 1

# save_custom_test_sets(datasets)


# --------------------------
# Define entry point
# --------------------------
def run_test(training_type, file_name):
    losses = []
    accuracies = []

    for i, dataset in enumerate(datasets):
        print('Testing (' + training_type +') -- ' + dataset_names[i])
        loss, accuracy = test(model_synergy, device, dataset)
        losses.append(loss)
        accuracies.append(accuracy)

    res_dir = os.path.join(os.getcwd(), 'results')

    row_labels = ['loss', 'accuracy']
    # write losses and accuracies to csv file
    write_results_csv(os.path.join(res_dir, file_name) + '.csv', training_type, dataset_names, row_labels, [losses, accuracies])

    # writing accuracy by class for each dataset
    for i, dataset in enumerate(datasets):
        check_class_accuracy(model_synergy, datasets[i], os.path.join(res_dir, file_name) + '.csv', dataset_names[i])


def run_second_training_cycle(net_type):
    # Set negative features
    model_synergy.net_type = net_type
    # Reset the fully connected layers
    model_synergy.fc1 = nn.Linear(320, HIDDEN).cuda()
    model_synergy.fc2 = nn.Linear(HIDDEN, 10).cuda()
    # Freeze convolutional layers
    model_synergy.conv1.weight.requires_grad = False
    model_synergy.conv2.weight.requires_grad = False
    # Reinitilaze the optimizer with new parameters
    optimizer_synergy = optim.SGD(filter(lambda p: p.requires_grad, model_synergy.parameters()), lr = LR, momentum = MOM)

    # Train the negative network (with positive features)
    for epoch in range(1, 10 + 1):
        train(model_synergy, device, train_loader, optimizer_synergy, epoch)

def main():
    # train network using synergy
    print("Started synergy training...")
    for epoch in range(1, 10 + 1):
        train(model_synergy, device, train_loader, optimizer_synergy, epoch)

    # test the trained network
    run_test('synergy', 'synergy')

if __name__ == '__main__':
    print("\nRunning from main.\n\n")
    main();