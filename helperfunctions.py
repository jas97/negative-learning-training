import torch
from torchvision import datasets, transforms
import numpy as np
import os
from loader import TrainLoader
from blur import GaussianSmoothing

# writes accuracy and loss items to csv file
# results - list of lists, each written in a new row
def write_results_csv(filename, title, column_labels, row_labels, results) :
    import csv
    with open(filename, 'a+') as csv_file:
        writer = csv.writer(csv_file, quoting=csv.QUOTE_MINIMAL)
        writer.writerow([title])
      
        first_row = ['*']
        first_row.extend(column_labels)
        writer.writerow(first_row)
      
        for i in range(1, len(results) + 1) :
            row_label = row_labels[i - 1]
            row = [row_label]
            row.extend(results[i - 1])
            writer.writerow(row)


# Checking accuracy for each class
def check_class_accuracy(model, test_loader, filename, dataset_name) :
    test_loss = 0.0
    class_correct = list(0. for i in range(10))
    class_total = list(0. for i in range(10))
  
    model.eval()
    with torch.no_grad() :
        for data, target in test_loader:
            data, target = data.cuda(), target.cuda()
            
            # class outputed by the model
            prediction = model(data).max(1, keepdim=True)[1].item()
            # correct prediction
            label = target.item()
        
            if (prediction == label) : 
                class_correct[label] += 1
            
            class_total[label] += 1

    accuracies = (np.asarray(class_correct) / np.asarray(class_total)) * 100.

    # row labels
    row_labels = ['total', 'correct', 'accuracy']
    
    # write accuracies to csv file
    write_results_csv(filename, dataset_name, 
                    ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'], row_labels, [class_total, class_correct, accuracies])



use_cuda = torch.cuda.is_available()
# print(f"\n\nUse cuda?{use_cuda}\n\n")
device = torch.cuda.device('cuda' if use_cuda else 'cpu')
kwargs = {'num_workers': 12, 'pin_memory': True} if use_cuda else {}

def mnist_loader(train=False):
    return torch.utils.data.DataLoader(
        datasets.MNIST('../data', download=True, train=train, transform=transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize((0.1307,), (0.3081,))
        ])),
        batch_size=64 if train else 1, shuffle=False, **kwargs)


def save_custom_test_sets(datasets):
    i = 0
    if not os.path.exists(os.path.join(os.getcwd(), 'datasets')):
        os.makedirs(os.path.join(os.getcwd(), 'datasets'))
    for dtst in datasets:
        if not os.path.exists(os.path.join(os.getcwd(), 'datasets', str(i) + '.pt')):
            print("Saving dataset " + str(i))
            with open(os.path.join(os.getcwd(), 'datasets', str(i) + '.pt'), "wb+") as f:
                new_test_set = (
                        dtst.dataset.test_data,
                        dtst.dataset.test_labels
                    )
                torch.save(new_test_set, f)
                i = i + 1


def fetch_custom_test_sets():
    datasets = []
    datasets_dir = os.path.join(os.getcwd(), 'datasets')
    if os.path.exists(datasets_dir):
        for file in os.listdir(datasets_dir):
            # TODO: Is transforms.Normalize((0.1307,), (0.3081,)) needed?
            datasets.append(
                torch.utils.data.DataLoader(
                    TrainLoader(os.path.join(datasets_dir, file), transforms.Compose([
                            transforms.ToTensor(),
                            transforms.Normalize((0.1307,), (0.3081,))
                        ]))
                    )
                )
        return datasets
    else: 
        '''
        We need to generate new test sets, because they don't exist on the disk
        '''
        test_loader = mnist_loader()
        test_loader_vertical_cut = mnist_loader()
        test_loader_horizontal_cut = mnist_loader()
        test_loader_diagonal_cut = mnist_loader()
        test_loader_triple_cut = mnist_loader()
        test_loader_triple_cut_noise = mnist_loader()
        test_loader_triple_cut_replaced1 = mnist_loader()
        test_loader_triple_cut_replaced3 = mnist_loader()
        test_loader_triple_cut_blur = mnist_loader()

        print('Generating new test sets...')

        smoothing = GaussianSmoothing(1, 5, 1)

        for num in tqdm(range(0, 10000)):
            random_pairs = get_random_pairs(test_loader.dataset.test_labels[num])

            sample = test_loader_triple_cut_blur.dataset.test_data[num].type('torch.FloatTensor')
            sample = F.pad(sample.reshape(1, 1, 28, 28), (2, 2, 2, 2), mode='reflect')
            blur = smoothing(sample).reshape(28, 28)

            for x in range(28):
                for y in range(28):
                    if y < 14:
                        test_loader_vertical_cut.dataset.test_data[num, x, y] = 0
                    if x < 14:
                        test_loader_horizontal_cut.dataset.test_data[num, x, y] = 0
                    if (x < 14 and y > 14) or (x > 14 and y < 14):
                        test_loader_diagonal_cut.dataset.test_data[num, x, y] = 0
                    if (5 < x < 15 and 5 <  y < 15) or (17 < x < 27 and 10 < y < 20) or (7 < x < 17 and 16 < y < 26):
                        test_loader_triple_cut.dataset.test_data[num, x, y] = 0
                        test_loader_triple_cut_noise.dataset.test_data[num, x, y] = random.randint(0, 255)
                        test_loader_triple_cut_replaced1.dataset.test_data[num, x, y] = test_loader.dataset.test_data[
                            random_pairs[0], x, y]
                        test_loader_triple_cut_blur.dataset.test_data[num, x, y] = blur[x, y]

                        if 5 < x < 15 and 5 < y < 15:
                            test_loader_triple_cut_replaced3.dataset.test_data[num, x, y] = test_loader.dataset.test_data[
                                random_pairs[0], x, y]
                        elif 17 < x < 27 and 10 < y < 20:
                            test_loader_triple_cut_replaced3.dataset.test_data[num, x, y] = test_loader.dataset.test_data[
                                random_pairs[1], x, y]
                        elif 7 < x < 17 and 16 < y < 26:
                            test_loader_triple_cut_replaced3.dataset.test_data[num, x, y] = test_loader.dataset.test_data[
                                random_pairs[2], x, y]

        print('\nTest sets generated...\n')
        datasets = [test_loader, test_loader_horizontal_cut, test_loader_vertical_cut,
                    test_loader_diagonal_cut, test_loader_triple_cut, test_loader_triple_cut_blur,
                    test_loader_triple_cut_noise, test_loader_triple_cut_replaced1,
                    test_loader_triple_cut_replaced3]

        save_custom_test_sets(datasets)
        return datasets



def get_random_pairs(label):
    while True:
        pairs = []
        while len(set(pairs)) != 3:
            pairs = [random.randint(0, 10000 - 1) for i in range(3)]

        got_duplicate_label = False
        for pair in pairs:
            if test_loader.dataset.test_labels[pair] == label:
                got_duplicate_label = True

        if got_duplicate_label:
            continue
        else:
            return pairs