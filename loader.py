import torch
from torch.utils.data.dataset import Dataset
import numpy as np
from PIL import Image

class TrainLoader(Dataset):

    def __init__(self, location, transform=None):
        '''
        TODO: Is a transform needed here?
        '''
        
        self.location = location
        self.transform = transform

        self.data, self.targets = torch.load(location)


    def __getitem__(self, index):
        img, target = self.data[index], int(self.targets[index])

        img = Image.fromarray(img.numpy(), mode='L')

        if self.transform is not None:
            img = self.transform(img)

        return img, target


    def __len__(self):
        return len(self.data)